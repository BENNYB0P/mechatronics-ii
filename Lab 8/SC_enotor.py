# -*- coding: utf-8 -*-
"""
@file SC_enotor.py

@package term_prj

@brief Motor and Encoder Drivers (Encoder + Motor = enotor)

@details This code contains the source code for both the encoder and motor driver
classes that are elemental in the term project. This drivers were developed last term
in ME 305, however, I have made some adjustments to make them more robust in my 
ME 405 term project. The classes are designed to set up objects
quickly. The imput parameters for both are documented in the initialization (init) function of each.
\n\n This motor class is how I quickly set up motor objects which are attached to a breakout board
equipped with an H bridge from Texas Instruments. There are 5 pins to set up in PWM that send high() or low()
signals. The H bridge then can send a sequence of DC motor functions to each motor. These functions include: \n
Motor Coast \n Motor Brake \n Motor Forward \n Motor Reverse \n\n The H bridge can control two motors, so we will set
up the system to do both!
\n\n This encoder class provides the tools necessary to create an object that can read
data from a motor's encoder. The code utilizes pyb and PWM pin functionality
that are built into Python. The encoder class uses timers to continously track the
position of the motor pairing in fast times.
\n\n Source code: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%208/SC_enotor.py


@date March 5th, 2021

@author Ben Presley
"""

#------------------------------IMPORT LIBRARIES------------------------------#
import pyb
import micropython
import utime

#---------------------------IMPORT SYSTEM VARIABLES--------------------------#
SystemFault = False
#---------------------------------MOTOR DRIVER-------------------------------#
class MotorDriver:
    ''' @brief Driver for DC motor
    @details This motor class is how I quickly set up motor objects which are attached to a breakout board
    equipped with an H bridge from Texas Instruments. There are 5 pins to set up in PWM that send high() or low()
    signals. The H bridge then can send a sequence of DC motor functions to each motor. These functions include: \n
    Motor Coast \n Motor Brake \n Motor Forward \n Motor Reverse \n\n The H bridge can control two motors, so we will set
    up the system to do both!
    ''' 
    
    #Set up pins for my breakout baord
        #nSLEEP = PA_15 = A15
        #IN1 = PB_4 = B4
        #IN2 = PB_5 = B5
        #IN3 = PB_0 = B0
        #IN4 = PB_1 = B1
        
        
        
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, ch1, ch2, nFAULT_pin):
        '''
        @brief Initializes the motor object from motorDriver class
        @details Sets up the pins and other parameters of the object.
        
        @param nSLEEP_pin hardware pin for nSLEEP functionality
        @param IN1_pin hardware pin for IN1 H-bridge functionality
        @param IN2_pin hardware pin for IN2 H-bridge functionality
        @param timer Timer object for PWM functionality
        @param ch1 PWM channel 1
        @param ch2 PWM channel 2
        @param nFAULT_pin hardware pin for nFAULT functionality
        '''
        #initalize pin objects
        self.IN1_pin = pyb.Pin (IN1_pin)
        self.IN2_pin = pyb.Pin (IN2_pin)
        
        #Define Channels
        self.ch1 = ch1
        self.ch2 = ch2
        
        #Initialize Ultra-precise Timer
        self.myTimer = pyb.Timer(timer, freq = 1000)
        
        #Assign nSLEEP
        if (nSLEEP_pin == None):
            print('H-Bridge already initialized')
        else:
            self.nSLEEP_pin = pyb.Pin (nSLEEP_pin)
            self.nTimer = pyb.Timer(2, freq = 1000)
            self.nSLEEPchannel = self.nTimer.channel(1,pyb.Timer.PWM, pin=self.nSLEEP_pin, pulse_width_percent = 100) #set nSLEEP pin high
            #Initialize the user button!
            self.myFAULT = pyb.ExtInt(nFAULT_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.FaultDetected)
                #The H-bridge's nFAULT pin needs to be defined in parameter properly
                #The button has a falling edge when triggered (pyb.ExtInt.IRQ_FALLING)
                #Enable the pull up resistor (pyb.Pin.PULL_UP)
                #Declare the timeChecker function to run on the interrupt!
            # Allocate external interrupt memory:
            micropython.alloc_emergency_exception_buf(2048)
        self.Coast()
    
    def AutoDuty(self, Duty, CurrentSpeed):
        '''@brief Allows the PWM input to be negative
        @details This function allows the PWM input to be negative, in which case
        it sends PWM signals in reverse at 100% duty. This saves me a big headache
        when I will eventually design a control system with these objects.
        In addition, if the motor is spinning the opposite direction, then it
        will apply a brake before applying the PWM to prevent saturation and/or
        conflict.'''
        
        #we need to make sure we dont punish the motor to reverse to much
        if(CurrentSpeed >= 0 and Duty >= 0) or (CurrentSpeed <= 0 and Duty <= 0):
            if(Duty == 0):
                self.Coast()
                
            elif(Duty<0):
                self.Reverse(Duty)
                
            elif(Duty>0):
                self.Forward(Duty)
        
        else:
            self.Brake()
                
            if(Duty<0):
                self.Reverse(Duty)
                
            elif(Duty>0):
                self.Forward(Duty)
            
    def Forward(self, Duty):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = Duty)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = 0)
        # print('Motor Spinning in Forward motion')
        
    def Reverse(self, Duty):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = 0)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = -(Duty))
        # print('Motor Spinning in Reverse motion')
    
    def Coast(self):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = 0)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = 0)
        # print('Motor Coasting')
        
    def Brake(self):
        self.IN1 = self.myTimer.channel(self.ch1,pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent = 100)
        self.IN2 = self.myTimer.channel(self.ch2,pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent = 100)
        # print('Motor applied Brakes')
        
    def Disable(self):
        self.nSLEEPchannel = self.nTimer.channel(1,pyb.Timer.PWM, pin=self.nSLEEP_pin, pulse_width_percent = 0) #set nSLEEP pin low
        # print('Motor has been put to sleep.\n Command "Motor.Wake" to resume.')
    
    def Enable(self):
        self.nSLEEPchannel = self.nTimer.channel(1,pyb.Timer.PWM, pin=self.nSLEEP_pin, pulse_width_percent = 100) #set nSLEEP pin high
        # print('Motor has been awoken.')
        
    def FaultDetected(which_pin):
        '''
        @brief This function runs when the H-bridge triggers a fault
        @details This is the external interrupt function that runs when the
        H-bridge sends a nFAULT low to the Nucleo. This function will disable
        the motor instantly by setting the global system fault variable to 
        True, which prevents outputs from the control system and will trigger the brake function. 
        The function will also alert the PC (Spyderspace) with a print message.
        '''
        print('FAULT WAS DETECTED IN H-BRIDGE')
        global SystemFault
        SystemFault = True
            


#--------------------------------ENCODER DRIVER------------------------------#

class EncoderDriver:
    '''
    @brief Communicates with hardware to get encoder data 
    @details This driver is key for accessing data from the motor's encoder
    which will be key for many projects in this course/profession.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1 - Read Timer
    S1_UPDATE           = 1    
    
    ## Constant defining State 2 - Interpret UPDATE
    S2_getDelta         = 2    
    
    ## Constant defining State 3 - Return Position
    S3_getPos           = 3
    
    ## Constant defining State 4 - Turn LED off
    S4_setPos           = 4
    
    
    def __init__(self, pin1, pin2, enc_timer, ch1, ch2, gear_ratio):
        '''
        @brief Established the parameters of the system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ##Define timer and pins
        self.pin1 = pin1
        self.pin2 = pin2
        self.enc_timer = enc_timer
        self.ch1 = ch1
        self.ch2 = ch2
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## Define Gear Ratio to get deg/tick
        self.gear_ratio = gear_ratio # [deg/tick]
        
        self.curr_pos = 0 #original location
        self.Delta = 0 #orininal delta
        self.newDelta = 0 #original newdelta
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + 200
        
        ## Set recording data state to False
        self.recording = 0
        
        ## Recording Resolution
        self.Resolution = 30 #[ms], please do not go below 50 ms.
        
        # Define Delta Interval
        self.deltaInterval = 15
        
        # Define Metrics
        self.curr_speed = 0
        self.curr_pos = 0
        
    def run(self):
        '''
        @brief Tasks run within state-machine and allow for multitasking
        @details The encoder must account for overflow and underflow. The
        program takes advantage of the timer function for pyb. The mode was 
        set to ENC_AB for easiest implementation of clockwise and
        counterclockwise movements. Note that the position is still just the
        ticks of the timer. The ticks have not been calibrated to degrees yet.
        The code also has a recording function built in!
        '''
        # State time conditions
        self.curr_time = pyb.millis()
        
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code - set Up Timer!
                self.tim = pyb.Timer(self.enc_timer)
                self.tim.init(prescaler=0,period=0xFFFF)
                self.tim.channel(self.ch1,pin=self.pin1,mode=pyb.Timer.ENC_AB)
                self.tim.channel(self.ch2,pin=self.pin2,mode=pyb.Timer.ENC_AB)
                self.oldTick = self.tim.counter() #original location
                self.curr_speed = 0 #must define now
                self.transitionTo(self.S1_UPDATE)

                
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code - Get info from Timer
                self.newTick = self.tim.counter() #simply snag data

                #Compute good or bad delta
                self.Delta = self.newTick - self.oldTick
                if(self.Delta == 0):
                    self.newDelta = 0
                elif(self.Delta < (-0xFFFF/2)): #overflow case
                    self.newDelta = (0xFFFF - self.oldTick) + self.newTick
                elif(self.Delta > (0xFFFF/2)): # underflow case
                    self.newDelta = -((0xFFFF - self.newTick) + self.oldTick)
                else: #normal case
                    self.newDelta = self.Delta #ticks
                
                #Define new position
                self.curr_pos = self.curr_pos + self.newDelta #ticks
                
                #Define new speed
                self.curr_speed = self.newDelta*self.gear_ratio*1000/self.deltaInterval/6 #rpm
                
                ## -- RECORDING DATA -- ##
                if(self.recording == 1):
                    if(self.curr_time - self.Resolution >= self.record_tlast): #record at resolution
                        print(str((self.curr_time - self.record_tinit)/1000) + ', '+ str(self.curr_speed) + ', ' + str(self.curr_pos*self.gear_ratio))
                        self.record_tlast = self.curr_time
                        
                    if((self.curr_time - self.record_tinit) > 15000): #after ten seconds of recording
                        self.recording = 0 #set recording to false
                        print('Recording Finished') #state the recording has stopped.
                        
                self.oldTick = self.newTick #redefine what the old tick is now
            self.next_time = self.curr_time + self.deltaInterval
            
    def getPos(self):
        '''
        @brief return current position
        '''
        return str('Current Postion: ' +str(self.curr_pos*self.gear_ratio))
    
    def setPos(self):
        '''@brief sets position to zero'''
        self.curr_pos = 0
        return str('Position reset to zero')
        
    def showDelta(self):
        '''
        @brief return current delta
        '''
        return str('Current Delta: '+str(self.Delta))
        
        
    def recordData(self):
        ''' @brief returns a data list of time vs position for 10 seconds'''
        self.recording = 1
        self.record_tinit = self.curr_time
        self.record_tlast = self.curr_time
        self.record_datainit = self.curr_pos
        print('Recording Started')


    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
#-------------------------------TEST CODE------------------------------------#
if __name__ == "__main__":
    #create the motor object with my motor driver
    myMotor1 = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 3, 1, 2)
    print('Motor 1 Initialized')
    
    #create the encoder object with my encoder driver
    myEncoder1 = EncoderDriver(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, 1, 2, 360/4000) #established encoder 1
    print('Encoder 1 Initialized')
    
    pyb.delay(25)
    
    t_init = utime.ticks_us()
    
    while True:
        curr_time = (utime.ticks_us() - t_init)/100000
        myEncoder1.run() #runs the encoder updates to keep em present
        
        if SystemFault == False and curr_time < 50:
            myMotor1.Forward(curr_time*2) #set PWM duty to the amount of seconds elapsed times two.
        
        pyb.delay(50) #let the motor speed up
        myEncoder1.showDelta()
        
