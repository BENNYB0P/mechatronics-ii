"""
@file Spyderspace_405.py

@package 405_Lab0x03

@brief Spyder script to interact real time with Nucleo

@details This scripts incorporates the serial functionality to communicate
with the Nucleo. It is a new and improved script that gives Spyder the ability to communicate
with the Nucluo in real time while recognizing user input on the user button
on the Nucleo device. In other words, the time sensitive controls have been transferred
over to the Nucleo's user button. The Nucleo is tasked to run the \ref main_405_L3.py script -- which
manages the analag to digital converter. The Nucleo then transmits the recorded data over
the serial port to Spyderspace. The recording function adds the bits of data to lists that
can be stored and plotted on the local computer for analysis and testing! \n
Source Code: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%203/Spyderspace_405.py
\n
Example Data output: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%203/output.csv
\n
This Spyderspace program communicated with the Nucleo and produced the following data
plot for voltage on the A6 pin on the Nucleo board. Please see \ref main_405_L3.py 
for the back end code. \n

\image html 405_lab3_test.png


@date February 4 2021

@author: Ben Presley
"""

import serial #for communication with Nucleo
import keyboard #for Spyder detecting keyboard presses.
import time #timekeeping
import matplotlib.pyplot as plt #for plotting
import sys #for exit function onlyg
import csv #for file saving

ser = serial.Serial(port='COM3',baudrate=115273,timeout=.1) #comms with Nucleo

    
## My FSM Spyder user interface class
class UIrequest:
    '''
    @brief User Interface for robust transfer of Inputs and Output 
    @details This code allows the user to input keystrokes to Spyder which sends commands
    to the Nucleo. The code will also respond to outputs from the Nucleo and 
    harvest/store data when necessary.
    '''
    
    ## Initialization state
    S0_INIT           = 0
    
    ## Communication state - runs forever
    S1_COMMUNICATE    = 1
    
    def __init__(self):
        '''
        @brief Established the parameters of the input system
        '''
        ## Set state to zero
        self.state = self.S0_INIT
        
        ## Run communication sequence every 10 ms
        self.Rate = 10
        
        ## The timestamp for the first iteration
        self.start_time = time.time()*1000
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.Rate
        
        #Set default target encoder/motor pair to 1
        self.Target = '1'
        
        #Make sure recording value is False
        self.Recording = False
        self.time_plot = [0, 0]
        self.volt_plot = [0, 0]
        self.AlreadyRecorded = False

        #Define old out and blanks
        self.oldout = 'GoPC'
        self.blank = ''
        
        ## Confirm code is running
        print('Spyderspace Engaged. Waiting for connection to Nucleo\n\nPress reset button on Nucleo to start...\n')
    
    def run(self):
        # State time conditions
        self.curr_time = time.time()*1000
        
        if self.curr_time > self.next_time:
            
            #State 0 Code - runs initially
            if(self.state == self.S0_INIT):
                self.checkgo = self.readLine()
                if(self.checkgo=='GoPC'):
                    print('Successful connection to Nucleo!')
                    print('\nAvailable functions:')
                    print('  g - gather voltage data')
                    print('  esc - Exit and plot & save data (if data was recorded)')
                    self.transitionTo(self.S1_COMMUNICATE)
                
            #State 1 Code - runs forever repeating
            elif(self.state == self.S1_COMMUNICATE):
                
                #Send any inputs to Nucleo if critical events are false
                if(self.Recording == False):
                    if(keyboard.is_pressed('g')): #get data for 10 seconds
                       if(self.AlreadyRecorded == False):
                           self.sendLine('g')
                       else:
                           print('You have already recorded data')

                    elif(keyboard.is_pressed('esc')):
                        ser.close()
                        sys.exit()


                # Recieve any outputs from Nucleo
                self.out = self.readLine()
                if(self.out != self.oldout and self.out != self.blank):
                    print(self.out)
                    
                    if(self.out == 'Recording Finished' or self.out == 'Recording Stopped'):
                        self.Recording = False
                        self.AlreadyRecorded = True
                        self.PlotResults(self.time_plot, self.volt_plot)
                        self.SaveResults(self.time_plot, self.volt_plot)
                        print('To exit and render results, press "esc"')
                        
                    if(self.Recording == True):
                        self.cleanstring = self.out
                        self.line_list = self.cleanstring.strip().split(', ')
                        try:
                            self.time_plot.append(float(self.line_list[0]))
                            self.volt_plot.append(float(self.line_list[1]))
                        except ValueError:
                            pass
                        
                        
                    if(self.out == 'Recording Started'):
                        self.Recording = True
                        
                self.oldout = self.out # define old data so we dont overprint it
            
            self.next_time = self.curr_time+self.Rate # run again at rate
            
        else:
            pass
                
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState
        
    def sendLine(self, string):
        '''
        @brief Sends a line of code through the serial bus
        @details I used ascii encoding because it seems to behave nicely with
        the back ends use of UART
        '''
        ser.flush()
        self.sendit = str(string).encode('ascii')
        ser.write(self.sendit)
            
        
    def readLine(self):
        '''
        @brief Recieves the most recent console line in the Nucleo
        @details I used -utf-8 decoding because it seemed to behave nicely with
        the front end (Spyder) and transfer large strings
        '''
        self.rawread = str(ser.readline().decode('-utf-8'))
        self.cleanread = self.rawread.strip('\r\n')
        return self.cleanread

    def PlotResults(self, xlist, ylist1):
        '''@brief plots given data series.'''
        x_vals = xlist
        y_vals1 = ylist1

        plt.figure(1)
        plt.plot(x_vals, y_vals1)

        plt.ylabel('Voltage, [V]')
        plt.xlabel('Time, [ms]')
        plt.title('Voltage on Pin A6')
        
    def SaveResults(self, xlist, ylist):
        '''@brief Saves two lists to two columns in csv format
        '''
        with open("output.csv", "w") as file:
            writer = csv.writer(file)
            writer.writerow(["Time, [ms]", "Voltage, [V]"])
            
            for row in zip(xlist, ylist):
                writer.writerow(row)

        
##--------------------------------------------------------------------------##

# Run the script from Spyder
myInterface = UIrequest()


for N in range(100000000): # effectively while(True):
    myInterface.run()
    
ser.close() #close serial connection properly
