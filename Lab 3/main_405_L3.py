# -*- coding: utf-8 -*-
"""
@file main_405_L3.py

@package 405_Lab0x03

@brief main script for \ref 405_Lab0x02 assignment. Runs timer interrupt.

@details This is the main code for Lab 3. It runs the serial communication through UART
to Spyder which picks up the most recent line printed by the Nucleo microcontroller. The code
is designed to meet tasks given in the Labratory 3 assignment handout. The goal is to read
voltage signals and communicate them back to the front end. The user button has been
introduced to the code to allow the user to start and stop recording using the microcontroller.
This allows more precise and accurate start and stop times because the button can
run on an external interrupt which is practically instantanous compared to a serial
command through Spyder. The Nucleo communicates the data through the serial port.

Source Code: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%203/main_405_L3.py

@date February 4 2021

@author Ben Presley

@copyright No use without written consent from Ben Presley.
"""


## Import necessary published libraries
import pyb #useful library
from pyb import UART #for communication with Spyder
import micropython #useful library for these sorts of things :)


## Define Serial Port for Handshake
myuart = UART(2)

## Send go ahead to PC
print('GoPC') #This string will prompt Spyder to move forward
pyb.delay(500) #give Spyder a second to transfer into operation state
                

##-----------------------------Input Setup----------------------------------##
lastInput = '-' #last input is null
target = 0  #target for data (which encoder/motor pair)
data1 = 0.0 #channel 1 for data input
data2 = 0.0 #channel 2 for data input
PC_wants_Data = False
Rec_Status = False
Rec_Resolution = 10 #[ms], resolution of voltage recording in milliseconds
rec_t_last = 0 #[ms], no recording so no time last recorded!

## Set up LED object to show initilization has been completed!
myLED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
myLED.high()

##-------------------------Interrupt function-------------------------------##

# Allocate external interrupt memory
micropython.alloc_emergency_exception_buf(2048)

def userButtonPressed(which_pin):
    '''
    @brief This function runs when the button is pressed
    @details This is the external interrupt function that runs when the
    user button is pressed on the Nucleo. The button is designed, in our case during
    this lab, to trigger the PC to start harvesting data from the Nucleo's wonderful
    voltage measuring capabilities!
    '''
    global PC_wants_Data
    global Rec_Status
    if PC_wants_Data == True and Rec_Status == False: #make sure the global variable is true 
                              #and that the Nucleo isnt recording!
        global record_init 
        record_init = pyb.millis()
        print('Recording Started') #Spyder recognizes this and will start recording!
        Rec_Status = True
        
    elif Rec_Status == True: #if already recording
        PC_wants_Data = False
        Rec_Status = False
        pyb.delay(5)
        print('Recording Stopped')


#Initialize the user button!
myButton = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, userButtonPressed)
    #The blue user button is bound to pin C13 (pyb.Pin.cpu.C13)
    #The button has a falling edge when triggered (pyb.ExtInt.IRQ_FALLING)
    #Enable the pull up resistor (pyb.Pin.PULL_UP)
    #Declare the timeChecker function to run on the interrupt!
    
##---------------------Analog-to-Digital Converter Object-------------------##
myADC = pyb.ADC(pyb.Pin.board.PA6) #creates a ADC object on pin A6!


##----------------------Run Finite State Machine Tasks----------------------##
while True:
    #Note the current run time
    curr_time = pyb.millis()
    
    #check for any requests and decode if true
    if myuart.any() != 0:        
        
        #read goodies from Spyder/UART
        rawinput = myuart.readline()
        newInput = str(rawinput.decode('ascii'))
        
        if newInput != lastInput:
            if newInput == 'g': #if g is pressed
                print('\nPress user button (blue button) on Nucleo to initiate data transfer!\nAnd, press it again to stop recording!')
                PC_wants_Data = True
            
        lastInput = newInput #save last input so we do not repeat
    
    
    #Read the voltage on the analog pin
    curr_voltage = 3.3*myADC.read()/4095 #pulls the voltage from the object and converts on the spot
                                         #4095 reports a 3.3V connection, so thats the conversion
    
    if Rec_Status == True:
        if(curr_time-rec_t_last >= Rec_Resolution): #record no faster than resolution
            print(str(curr_time - record_init) + ', '+ str(curr_voltage))
            rec_t_last = curr_time
            


    