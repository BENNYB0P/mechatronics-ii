"""
@file main_405_L2.py

@package 405_Lab0x02

@brief main script for \ref 405_Lab0x02 assignment. Runs timer interrupt.

@details This code is elemental in understand how external interrupts work. In 
this labratory assignment, I have created a function that interrupts a timer instantly
to measure the reaction time of a user. The interrupt is triggered by the user button
on the microcontroller, and quickly snags the data from the timer. The process is constantly
available, and I have woven into this user interface. The user interface will reset if the user
forgets to click after a few seconds. The code runs forever!

Source Code: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%202/main_405_L2.py

@date January 27 2021

@author Ben Presley

@copyright No use without written consent from Ben Presley.

"""

#---------------------------import supporting files--------------------------#
import pyb
import micropython
import utime
import random

#----------------------------initialize hardware-----------------------------#
# Define the MCU's cpu clock speed
mcuClock = 80000000 # [Hz]

# Set up timer object
myTime = pyb.Timer(2)
myTime.init(prescaler = 0, period = 0x7FFFFFFF)
myClock = mcuClock # [Hz], define the timers clock speed

## Set up LED object!
myLED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
myLED.low()


#--------------------------interrupt functionality---------------------------#
# Allocate external interrupt memory
micropython.alloc_emergency_exception_buf(256)

# Interrupt function
def timeChecker(which_pin):
    '''
    @brief This function interrupts the timer function
    @details The function is triggered by a pin external interrupt, and asks the
    timer for its count. The function is very barebones since it has to stay within a few hundred
    bytes of memory capacity. The function simply sets a global variable to the status of the
    counter at that very instant!
    '''
    quick_pull = myTime.counter() #do not waste time figuring out global variable.
    global response_time
    response_time = quick_pull #now assign it

#Initialize the external interrupt command
myButton = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, timeChecker)
    #The blue user button is bound to pin C13 (pyb.Pin.cpu.C13)
    #The button has a falling edge when triggered (pyb.ExtInt.IRQ_FALLING)
    #Enable the pull up resistor (pyb.Pin.PULL_UP)
    #Declare the timeChecker function to run on the interrupt!


def testReset():
    '''
    @brief This function resets the timer and variables
    @details The function is responsible for setting variables back to pre-testing
    state where the LED is off and the counter is reset to zero to prevent overloading it.
    '''
    myLED.low()
    myTime.counter(0)
    global testing
    testing = False
    utime.sleep(.5) #give 'em some rest


#------------------------------main file-------------------------------------#

testing = False # set testing conditions to default
response_time = None # declare the response time global variable

while True: #run forever
    
    if testing == False:
        
        print('Welcome to the response time test! Test starts now')
        utime.sleep(random.uniform(1.0,3.0))
        testing = True
        response_time = None
        myLED.high()
        myTime.counter(0)
    
    if testing == True:
        
        if response_time != None:
            elapsed = (response_time/myClock)*1000
            print('Your response time was:',elapsed,'milliseconds \n ')
            testReset()
            
        if myTime.counter() > (4*myClock): #after a few seconds.. restart!
            print('Hello? We will restart the test for you now... \n ')
            testReset()