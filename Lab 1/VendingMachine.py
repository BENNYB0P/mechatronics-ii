"""
@file VendingMachine.py

@package 405_Lab0x01

@brief Script for \ref 405_Lab0x01 assignment. Runs vending machine.

@details This script contains a Python function that computes the
correct change for a given purchase. This function takes in a numerical value
representing the cost of an item, along witha tuple containing the denominations
being used for the purchase. The function then returns change in the fewest
denominations possible.

Source Code: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%201/VendingMachine.py

@date January 18 2021

@author Ben Presley

@copyright No use without written consent from Ben Presley.
"""

import keyboard
import time

def countChange(payment):
    '''
    @brief This function determines amount of currency in bill qty. tuple.
    @details The function is responsible for converting a quanitiy of bills and
    coins into a useful integer representing how much money is represented by
    the quanity of tender in the tuple. The following format must be used for
    for the parameter:
    \n The payment should be imported into the function as a size 8 tuple, with the following format which
    states the quantity of each type of currency:
    \n payment = [pennies, nickels, dimes, quarters, $1 bill, $5 bill, $10 bill, $20 bill] \n\n
    
    @param payment The tuple containing quanitity of coins and bills.
    
    @returns An integer number of cents
    
    '''
    #Interpret the payment tuple input!
    counted_money = 1*payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment[5] + 1000*payment[6] + 2000*payment[7] # + 50*payment[8] + 100*payment[9]
    return counted_money

def getChange(remaining_balance):
    '''
    @brief Function that returns correct change for cash purchases
    @details The function takes a payment in the form of a tuple and calculates the
    difference between the price and amount paid and returns the best way to return change!
    If the user just wants the best way to represent money as cash, the price can be set to
    a negative value and set the payment tuple to all zeroes!
    \n\n The payment should be imported into the function as a size 8 tuple, with the following format which
    states the quantity of each type of currency:
    \n payment = [pennies, nickels, dimes, quarters, $1 bill, $5 bill, $10 bill, $20 bill] \n\n
    If the payment is not formatted properly the program will not operate! \n\n
    The program is also smart enough to know if the customer is not paying enough money and
    will return None.
    
    @param remaining_balance The credit remaining in cents (integer).
    
    @returns A tuple with best change for customer, following the same format as the payment
    '''
    myChange = remaining_balance
    
    if myChange < 0: #error handling
        return None
    else:
        pass

    #Run the Change Algorithm
    twenties = int(myChange // 2000)
    myChange = myChange % 2000
    tens = int(myChange // 1000)
    myChange = myChange % 1000
    fives =  int(myChange // 500)
    myChange = myChange % 500
    ones = int(myChange // 100)
    myChange = myChange % 100
    quarters = int(myChange // 25)
    myChange = myChange % 25
    dimes = int(myChange // 10)
    myChange = myChange % 10
    nickels = int(myChange // 5)
    myChange = myChange % 5
    pennies = int(myChange // 1)
    
    #Build new tuple with algorithm results
    myTender = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    
    return myTender

def printWelcome():
    '''@brief Write an introduction screen for the vending machine
    @details The function prints a welcome message to the user, and shows
    beverage choices and their prices!
    '''
    print('Welcome to the Quenchotron \nThe beverages choices are:')
    print('Cuke        -  $1.69')
    print('Popsi       -  $2.09')
    print('Spryte      -  $1.89')
    print('Dr. Pupper  -  $2.29')
    

#--------------------- Run the Finite State Machine --------------------------#
state = 0 #start the FSM at state 0 :)
debounce_time = .1 #this should be enough time for the user to press and lift off of a key

while True:
    
    # Run state 0 - Initialize
    if state == 0:
        cash_inserted = (0, 0, 0, 0, 0, 0, 0, 0) # 4 quarters and 1 $1 bill and 1 $5 bill
        cuke_price = 169 #define price of Cuke
        popsi_price = 209 #define price of Popsi
        spryte_price = 189 #define price of Spryte
        pupper_price = 229 #define price of Spryte
        state = 1 #define the next state to run (state 1 is homebase)
        credit = 0 #define this variable before code runs
        subtotal = 0 #define this variable before code runs
        new_funds = True #define this variable before code runs
        printWelcome() #print the welcome screen

    # Run state 1 - Detect Inputs and Update LCD/console display
    if state == 1:
        if new_funds == True: #only show price if it has changed. (Console control)
            print('Current credit: $', (float(credit)/100)) #convert integer cents to float dollars.
            new_funds = False #set the relevancy back to false/unchanged.
            
        if keyboard.is_pressed('e'): #Eject
            state = 5 #transition to eject state
            time.sleep(debounce_time) #temporary keyboard debounce fix (where it logs multiple key strokes)
        
        elif keyboard.is_pressed('c'): #Cuke
            subtotal = cuke_price #define subtotal based on selection
            selection = 'c' #save user selection
            time.sleep(debounce_time) #temporary keyboard debounce fix
            state = 2 #transition to read selection!
            
        elif keyboard.is_pressed('p'): #Popsi
            subtotal = popsi_price #define subtotal based on selection
            selection = 'p' #save user selection
            time.sleep(debounce_time) #temporary keyboard debounce fix
            state = 2 #transition to read selection!
            
        elif keyboard.is_pressed('s'): #Spryte
            subtotal = spryte_price #define subtotal based on selection
            selection = 's' #save user selection
            time.sleep(debounce_time) #temporary keyboard debounce fix
            state = 2 #transition to read selection!
            
        elif keyboard.is_pressed('d'): #Spryte
            subtotal = pupper_price #define subtotal based on selection
            selection = 'd' #save user selection
            time.sleep(debounce_time) #temporary keyboard debounce fix
            state = 2 #transition to read selection!
        
        elif keyboard.is_pressed('0'): #Penny inserted
            state = 6
            cash_inserted = (cash_inserted[0] +1, cash_inserted[1], cash_inserted[2], cash_inserted[3], cash_inserted[4], cash_inserted[5], cash_inserted[6], cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
        
        elif keyboard.is_pressed('1'): #Nickel inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1] +1, cash_inserted[2], cash_inserted[3], cash_inserted[4], cash_inserted[5], cash_inserted[6], cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
        elif keyboard.is_pressed('2'): #Dime inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1], cash_inserted[2] +1, cash_inserted[3], cash_inserted[4], cash_inserted[5], cash_inserted[6], cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
        elif keyboard.is_pressed('3'): #Quarter inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1], cash_inserted[2], cash_inserted[3] +1, cash_inserted[4], cash_inserted[5], cash_inserted[6], cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
        elif keyboard.is_pressed('4'): #Dollar bill inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1], cash_inserted[2], cash_inserted[3], cash_inserted[4] +1, cash_inserted[5], cash_inserted[6], cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
        elif keyboard.is_pressed('5'): #5 Dollar bill inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1], cash_inserted[2], cash_inserted[3], cash_inserted[4], cash_inserted[5]+1, cash_inserted[6], cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
        elif keyboard.is_pressed('6'): #10 Dollar bill inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1], cash_inserted[2], cash_inserted[3], cash_inserted[4], cash_inserted[5], cash_inserted[6]+1, cash_inserted[7])
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
        elif keyboard.is_pressed('7'): #20 Dollar bill inserted
            state = 6 #transition to update price state
            cash_inserted = (cash_inserted[0], cash_inserted[1], cash_inserted[2], cash_inserted[3], cash_inserted[4], cash_inserted[5], cash_inserted[6], cash_inserted[7]+1)
            time.sleep(debounce_time) #temporary keyboard debounce fix
            
    # Run state 2 - Read and validate selection        
    if state == 2:
        if subtotal > credit: #make sure there is enough credit!
            print('Sorry - not enough funds.')
            new_funds = True #make sure the user knows/sees their credit!
            state = 1 #go back to waiting for inputs!
        else:
            new_funds = True #show the new balance
            state = 3 #pass to dispensing state
    
    # Run state 3 - Dispense and Subtract credit
    if state == 3:
        if selection == 'c': #Dispense the correct drink based on selection!
            print('Dispensing your Cuke! Enjoy!')
            credit = credit - cuke_price
        elif selection == 's':
            print('Dispensing your Spryte! Enjoy!')
            credit = credit - spryte_price
        elif selection == 'p':
            print('Dispensing your Popsi! Enjoy!')
            credit = credit - popsi_price
        elif selection == 'd':
            print('Dispensing your Dr. Pupper! Enjoy!')
            credit = credit - pupper_price
        else: #error handling
            state = 1
        
        state = 4 #go to the next state
    
    # Run state 4 - Another?
    if state == 4:
        if credit > 0:
            print('You have remaining balance. Press eject (e) or continue shopping.')
            state = 1
        else: 
            print('Thank you for shopping at Quenchotron!')
            state = 1
            
    # Run state 5 - return change
    if state == 5:
        cash_to_return = getChange(credit)
        print('\nThank you for shopping at Quenchotron!\nHere is your change: ')
        #The following if statements only print the form of money that will be returned
        if cash_to_return[0] != 0:
            print('    pennies: ',cash_to_return[0])
        if cash_to_return[1] != 0:
            print('    nickels: ',cash_to_return[1])
        if cash_to_return[2] != 0:
            print('      dimes: ',cash_to_return[2])
        if cash_to_return[3] != 0:
            print('   quarters: ',cash_to_return[3])
        if cash_to_return[4] != 0:
            print('   $1 bills: ',cash_to_return[4])
        if cash_to_return[5] != 0:
            print('   $5 bills: ',cash_to_return[5])
        if cash_to_return[6] != 0:
            print('  $10 bills: ',cash_to_return[6])
        if cash_to_return[7] != 0:
            print('  $20 bills: ',cash_to_return[7])
    
        state = 0 #restart the user interface!!!
        print(' ')
        
    if state == 6:
        credit = countChange(cash_inserted) #run the counting change function
        new_funds = True #update the LCD console with new price!
        state = 1 #go back to state 1 - input waiting state
        

        