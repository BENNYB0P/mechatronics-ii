%% Link to my online portfolio and documentation!
% Source code found here: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Lab%206/Lab6_simspace.m

%% Housekeeping
clc; close all; clear;

%% Set up Symbolic Parameters
%The symbolic parameters of are system need to be defined first
syms m_b I_b %parameters related to ball
syms I_p l_p r_p m_p %parameter of platform
syms l_r r_b r_g r_c r_m %parameters related to the linkages
syms b g T_x %general parameters and input torque
syms x theta_y x_dot theta_dot_y %state vector

%% Import System Matrices
% Special thanks to my lecture instructors, they posted a robust and
% helpful guideline to starting this lab. Although there were struggles
% in last weeks calculations, I was able to get a head start on using
% Jacobian Analysis to solve this large system of dynamic equations.
% Find below the final M and f matrices that can be used to start 
% Lab 0x06. Note that the following matrices are not fully linearized. The 
% motor/platform relationship was developed with the small angle 
% assumption, but otherwise no linearization has been done yet. 

%define the M matrix (given on Piazza)
M = [-(m_b*r_b^2+m_b*r_c*r_b + I_b)/r_b, -(I_b*r_b+I_p*r_b+m_b*r_b^3+m_b*r_b*r_c^2+2*m_b*r_b^2*r_c+m_p*r_b*r_g^2+m_b*r_b*x^2)/r_b; -(m_b*r_b^2+I_b)/r_b, -(m_b*r_b^3 + m_b*r_c*r_b^2 + I_b*r_b)/r_b]

%define the f matrix (given on Piazza)
f = [b*theta_dot_y - g*m_b*(sin(theta_y)*(r_b+r_c) + x*cos(theta_y)) + T_x*l_p/r_m + 2*m_b*theta_dot_y*x*x_dot - g*m_p*r_g*sin(theta_y); -m_b*r_b*x*theta_dot_y^2 - g*m_b*r_b*sin(theta_y)]

%we can now easily solve for x_dd vector with inverse matrices division
x_dd = M^-1 * f

%% Utilize techniques of Jacobian Linearization
% Jacobian Linearzation is useful in this case because we have a non-linear
% dynamic system that needs to be linearized to solve a large and
% complicated ODE -- which fortunately has been given to us in a matrix
% form. The @50 piazza post was also very useful for my code development
% and process. Again, thank you again to my instructors.

%define the jacobian matrix
J = [x_dd(1); x_dd(2); x_dot; theta_dot_y]

%define the jacobian vector for the states
J_x = jacobian(J,[x_dot, theta_dot_y, x, theta_y])

%define the jacobian vector for the inputs
J_u = jacobian(J,T_x)

%% Perform symbolic substition and linearization
% By evaluating these matrices at a desired operating point x = x0 and 
% u = u0, we can come up with the A and B matrices for a standard 
% statespace representation.

%define all the symbolic parameters with that of our system
%these are given in the order shown in the Lab 0x06 handout.
r_m = 0.060; %[m]
l_r = 0.050; %[m]
r_b = 0.0105; %[m]
r_g = 0.042; %[m]
l_p = 0.110; %[m]
r_p = 0.0325; %[m]
r_c = 0.050; %[m]
m_b = 0.030; %[kg]
m_p = 0.400; %[kg]
I_p = 0.00188; %[kg-m^2]
b = 0.010; %[N-m-s/rad]

%this value was calculated (inerta of solid sphere)
I_b = 2/5*r_b^2*m_b; %[kg-m^2]

%gravity assumed standard
g = 9.81; %[m/s^2]

%as mentioned, set the desired conditions as the starting point
T_x = 0; %[N-m]
x = 0; %[m]
theta_y = 0; %[rad]
x_dot = 0; %[m/s]
theta_dot_y = 0; %[rad/s]

%finally, we can subsitute all of the new defined symbolic variables with
%their actual values with the subs function. Using double to limit the
%computational intensity. Standard Statespace Represenation
A = double(subs(J_x)); %statespace 
B = double(subs(J_u)); %inputspace
I = eye(4); %identity matrix for variables
H = zeros(4,1); %homogeneous system plz :)
model = ss(A,B,I,H); %define statespace model (going to use statespace
                     %functions from MATLAB control system toolbox)

%% Run Initial Condition Analysis

%----------------------------Scenario 3A----------------------------------%
time_3a = 1; %[s]
init_cond_3a = [0,0,0,0]; %[x_dot, theta_dot_y, x, theta_y]
[y_3a, t_3a, x_3a] = initial(model, init_cond_3a, time_3a);

figure(1);
subplot(2,2,1);
plot(t_3a, y_3a(:,1), 'k')
title('Linear Velocity for 3a')
xlabel('time, [sec]')
ylabel('Velocity, [m/s]')

subplot(2,2,2)
plot(t_3a, y_3a(:,2), 'k')
title('Angular Velocity for 3a')
xlabel('time, [sec]')
ylabel('Angular Velocity, [rad/s]')

subplot(2,2,3)
plot(t_3a, y_3a(:,3), 'k')
title('Linear Postion for 3a')
xlabel('time, [sec]')
ylabel('Position, [m]')

subplot(2,2,4)
plot(t_3a, y_3a(:,4), 'k')
title('Angular Position for 3a')
xlabel('time, [sec]')
ylabel('Angle, [rad]')

%----------------------------Scenario 3B----------------------------------%
time_3b = 0.4; %[s]
init_cond_3b = [0,0,0.05,0]; %[x_dot, theta_dot_y, x, theta_y]
[y_3b, t_3b, x_3b] = initial(model, init_cond_3b, time_3b);

figure(2);
subplot(2,2,1);
plot(t_3b, y_3b(:,1), 'k')
title('Linear Velocity for 3b')
xlabel('time, [sec]')
ylabel('Velocity, [m/s]')

subplot(2,2,2)
plot(t_3b, y_3b(:,2), 'k')
title('Angular Velocity for 3b')
xlabel('time, [sec]')
ylabel('Angular Velocity, [rad/s]')

subplot(2,2,3)
plot(t_3b, y_3b(:,3), 'k')
title('Linear Postion for 3b')
xlabel('time, [sec]')
ylabel('Position, [m]')

subplot(2,2,4)
plot(t_3b, y_3b(:,4), 'k')
title('Angular Position for 3b')
xlabel('time, [sec]')
ylabel('Angle, [rad]')

%----------------------------Scenario 3C----------------------------------%
time_3c = 0.4; %[s]
init_cond_3c = [0,0,0,deg2rad(5)]; %[x_dot, theta_dot_y, x, theta_y]
[y_3c, t_3c, x_3c] = initial(model, init_cond_3c, time_3c);

figure(3);
subplot(2,2,1);
plot(t_3c, y_3c(:,1), 'k')
title('Linear Velocity for 3c')
xlabel('time, [sec]')
ylabel('Velocity, [m/s]')

subplot(2,2,2)
plot(t_3c, y_3c(:,2), 'k')
title('Angular Velocity for 3c')
xlabel('time, [sec]')
ylabel('Angular Velocity, [rad/s]')

subplot(2,2,3)
plot(t_3c, y_3c(:,3), 'k')
title('Linear Postion for 3c')
xlabel('time, [sec]')
ylabel('Position, [m]')

subplot(2,2,4)
plot(t_3c, y_3c(:,4), 'k')
title('Angular Position for 3c')
xlabel('time, [sec]')
ylabel('Angle, [rad]')

%----------------------------Scenario 3D----------------------------------%
%This scenario looks different because we have an arbitrary input for
%scenarios like this. The time and inputs need to be specifified expicitly
%by myself with the impulse input occuring at the beginning of the time
%series.

time_3d = linspace(0, 0.4, 1000); %[s]
input_3d = zeros(1,length(time_3d)); %all are zero except
input_3d(1) = 1*10^-3/(.4/1000); %the first instant in the sequence
%note: the impulse of 1 mNm/s was converted to torque!

[y_3d, t_3d, x_3d] = lsim(model, input_3d, time_3d);

figure(4);
subplot(2,2,1);
plot(t_3d, y_3d(:,1), 'k')
title('Linear Velocity for 3d')
xlabel('time, [sec]')
ylabel('Velocity, [m/s]')

subplot(2,2,2)
plot(t_3d, y_3d(:,2), 'k')
title('Angular Velocity for 3d')
xlabel('time, [sec]')
ylabel('Angular Velocity, [rad/s]')

subplot(2,2,3)
plot(t_3d, y_3d(:,3), 'k')
title('Linear Postion for 3d')
xlabel('time, [sec]')
ylabel('Position, [m]')

subplot(2,2,4)
plot(t_3d, y_3d(:,4), 'k')
title('Angular Position for 3d')
xlabel('time, [sec]')
ylabel('Angle, [rad]')

%% Closed Loop Controller with full state feedback
% Using closed loop control will be essential for the term project, so lets
% start simulating it!

%define statespace control block, K (given in lab manual)
K = [-0.05 -0.02 -0.3 -0.2];
A_control = A - B*K;
model_control = ss(A_control,H,I,H);

%----------------------------Scenario 4-----------------------------------%

time_4 = 20; %[s]
init_cond_4 = [0,0,.05,0]; %[x_dot, theta_dot_y, x, theta_y]
[y_4, t_4, x_4] = initial(model_control, init_cond_4, time_4);

figure(5);
subplot(2,2,1);
plot(t_4, y_4(:,1), 'k')
title('Linear Velocity with Controller')
xlabel('time, [sec]')
ylabel('Velocity, [m/s]')

subplot(2,2,2)
plot(t_4, y_4(:,2), 'k')
title('Angular Velocity with Controller')
xlabel('time, [sec]')
ylabel('Angular Velocity, [rad/s]')

subplot(2,2,3)
plot(t_4, y_4(:,3), 'k')
title('Linear Postion with Controller')
xlabel('time, [sec]')
ylabel('Position, [m]')

subplot(2,2,4)
plot(t_4, y_4(:,4), 'k')
title('Angular Position with Controller')
xlabel('time, [sec]')
ylabel('Angle, [rad]')

