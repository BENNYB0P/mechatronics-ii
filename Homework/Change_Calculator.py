"""
@file Change_Calculator.py

@package HW_0x01

@brief Script for \ref HW_0x01 assignment. Counts change.

@details This script contains a Python function that computes the
correct change for a given purchase. This function takes in a numerical value
representing the cost of an item, along witha tuple containing the denominations
being used for the purchase. The function then returns change in the fewest
denominations possible.

Source Code: https://bitbucket.org/BENNYB0P/mechatronics-ii/src/master/Homework/Change_Calculator.py

@date January 10 2021

@author Ben Presley

@copyright No use without consent.
"""

def getChange(price, payment):
    '''
    @brief Function that returns correct change for cash purchases
    @details The function takes a payment in the form of a tuple and calculates the
    difference between the price and amount paid and returns the best way to return change!
    If the user just wants the best way to represent money as cash, the price can be set to
    a negative value and set the payment tuple to all zeroes!
    \n\n The payment should be imported into the function as a size 10 tuple, with the following format which
    states the quantity of each type of currency:
    \n payment = [pennies, nickels, dimes, quarters, $1 bill, $5 bill, $10 bill, $20 bill, $50 bill, $100 bill] \n\n
    If the payment is not formatted properly the program will not operate! \n\n
    The program is also smart enough to know if the customer is not paying enough money. This could be transferred out of the
    function within the return tuple, but that may be for another time! This works just fine for this homework!
    
    @param price The cost of the purchase in cents (integers only).
    
    @param payment A tuple of bills/coins that were used for payment.
    
    @returns A tuple with best change for customer, following the same format as the payment
    '''
    #Interpret the payment tuple input!
    myPayment = 1*payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment[5] + 1000*payment[6] + 2000*payment[7] # + 50*payment[8] + 100*payment[9]
    
    #Make sure the price is an integer of the cost in cents.
    myPrice = int(price)
    
    #Calculate the difference
    myChange = myPayment - myPrice
    
    if myChange < 0:
        myChange = -1*myChange
        print('\nI do not speak broke! Sorry! You still owe me $', myChange,'\n\nDo you have the following?:\n')
    else:
        pass
        
    
    #Run the Change Algorithm
    # hundreds = int(myChange // 100)
    # myChange = round(myChange % 100, 2)
    # fifties = int(myChange // 50)
    # myChange = round(myChange % 50, 2)
    twenties = int(myChange // 2000)
    myChange = myChange % 2000
    tens = int(myChange // 1000)
    myChange = myChange % 1000
    fives =  int(myChange // 500)
    myChange = myChange % 500
    ones = int(myChange // 100)
    myChange = myChange % 100
    quarters = int(myChange // 25)
    myChange = myChange % 25
    dimes = int(myChange // 10)
    myChange = myChange % 10
    nickels = int(myChange // 5)
    myChange = myChange % 5
    pennies = int(myChange // 1)
    
    myTender = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    
    return myTender

# Now lets run an example!

if __name__ == "__main__":
    cash_inserted = (0, 0, 0, 4, 1, 1, 0, 0) # 4 quarters and 1 $1 bill and 1 $5 bill

    price_of_item = 334

    cash_to_return = getChange(price_of_item, cash_inserted)

    #The following UI can be deleted if not necessary :)

    print('    pennies: ',cash_to_return[0])
    print('    nickels: ',cash_to_return[1])
    print('      dimes: ',cash_to_return[2])
    print('   quarters: ',cash_to_return[3])
    print('   $1 bills: ',cash_to_return[4])
    print('   $5 bills: ',cash_to_return[5])
    print('  $10 bills: ',cash_to_return[6])
    print('  $20 bills: ',cash_to_return[7])
    # print('  $50 bills: ',cash_to_return[8])
    # print(' $100 bills: ',cash_to_return[9])