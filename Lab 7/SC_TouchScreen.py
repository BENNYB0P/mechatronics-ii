"""
@file SC_TouchScreen.py

@package term_prj

@brief Lab 0x07 - Driver Class for Resistive Touch Screen

@details This is a driver for the resistive, 8-inch touch screen panel in my term project
for this advanced mechatronics course. This lab will be elemental in my control system
because it will track the ball's planar position on the self-balancing platform.
The resistive touch panel using X, Y and Z scanning to sense its position on the table.
\n The X axis is the lateral position
\n The Y axus is the vertical positon
\n The Z axis is simply of there is contact with the screen
\n Each axis is obviously very important to the control system, although this source
code could be used for almost any project where a resistive touch screen is required.
\n I was taught how a resistive touch screen works in class. In short, the resistance touch
screen can be considered a pair of potentiometers built into the screen, and when the screen
is pressed in a certain location, the volktage across each of those pairs changes uniquely
to each position that can be pressed. If we interpret the voltage changes across this pair,
then we can build a driver for the touchscreen. Where we get an exact position from the
analog voltage signals from the touchscreen. That is exactly what this file is for.

\n An image of the testing has been posted below, which shows a general testing scenario.
The testing involved printing values of each pin and converting the ADC data into
useful information. The photo shows the final tune of the optimized function. \n

\image html "touchtest.jpeg"

@date March 3rd, 2021

@author Ben Presley
"""
#------------------------------IMPORT LIBRARIES------------------------------#
import pyb
import utime

#------------------------------DESIGN THE DRIVER-----------------------------#
class TouchScreen:
    '''
    @brief This is a driver for a resistive touch screen
    @details This driver is designed to read touch screen values as fast as possible.
    The driver was developed for my term project in ME 405. This is a driver for the resistive, 8-inch touch screen panel in my term project
    for this advanced mechatronics course. This lab will be elemental in my control system
    because it will track the ball's planar position on the self-balancing platform.
    The resistive touch panel using X, Y and Z scanning to sense its position on the table.
    \n The X axis is the lateral position
    \n The Y axus is the vertical positon
    \n The Z axis is simply of there is contact with the screen
    \n Each axis is obviously very important to the control system, although this source
    code could be used for almost any project where a resistive touch screen is required.
    \n I was taught how a resistive touch screen works in class. In short, the resistance touch
    screen can be considered a pair of potentiometers built into the screen, and when the screen
    is pressed in a certain location, the volktage across each of those pairs changes uniquely
    to each position that can be pressed. If we interpret the voltage changes across this pair,
    then we can build a driver for the touchscreen. Where we get an exact position from the
    analog voltage signals from the touchscreen. 
    '''
    
    def __init__(self, x_p, x_m, y_p, y_m):
        '''
        @brief Initializes a touchscreen object
        @param x_p x+ pin as a pyb.Pin.cpu object
        @param x_m x- pin as a pyb.Pin.cpu object
        @param y_p y+ pin as a pyb.Pin.cpu object
        @param y_m y- pin as a pyb.Pin.cpu object
        '''
        # Set up the pins based on input parameters! We define these as is and will
        # change them for each 
        self.x_p = x_p #x plus analog pin
        self.x_m = x_m #x minus analog pin
        self.y_p = y_p #y plus analog pin
        self.y_m = y_m #y minus analog pin
        
        # Define parameters of the touch screen and ADC system
        self.edgeVoltage = 4096  # max ADC output would be at edge of touchscreen
        self.liftVoltage = 4000  # ADC output when touch pressed is false (had to tune this one)
        self.screenLength_x = 176  # length of screen in millimeters
        self.screenLength_y = 100  # length of screen in millimeters
        
        # Initialize the pin states (these toggle often for resistive touch screen scans)
        self.pin_high = None
        self.pin_low = None
        self.pin_read = None
        self.pin_highz = None
        
        # Initialize voltage and position correlation variables (for calculation)
        self.voltage = 0
        self.pos = 0
        
        # Initialize position data variables (results)
        self.x = 0
        self.y = 0
        self.z = False
        
    def scan_X(self):
        '''
        @brief Scans the x position of the touch
        
        @details This function scans and returns the x position of the touch. The pins
        need to be configured as follows to return the x position:
         -  high pin = x_p
         -   low pin = x_m
         - highz pin = y_m
         -  read pin = y_p
        '''
        # Shuffle pins to correct modes
        self.pin_high = pyb.Pin(self.x_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.y_p)
        self.pin_highz = pyb.Pin(self.y_m, mode = pyb.Pin.IN)
        self.pin_high.value(1) #set high pin to high
        self.pin_low.value(0)  #set low pin to low
        utime.sleep_us(4)      #3.6 microseconds to settle - based on lab handout!
        self.voltage =  self.pin_read.read()
        #perform position calculation with linear calculations/coeffecients
        self.pos = int(self.voltage/self.edgeVoltage*self.screenLength_x - self.screenLength_x/2)
        return self.pos
        
    def scan_Y(self):
        '''
        @brief Scans the y position of the touch
        
        @details This function scans and returns the y position of the touch. The pins
        need to be configured as follows to return the y position: 
         -  high pin = y_p
         -   low pin = y_m
         - highz pin = x_m
         -  read pin = x_p
        '''
        # Shuffle pins to correct modes
        self.pin_high = pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.y_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.x_p)
        self.pin_highz = pyb.Pin(self.x_m, mode = pyb.Pin.IN)
        self.pin_high.value(1) #set high pin to high
        self.pin_low.value(0)  #set low pin to low
        utime.sleep_us(4)      #3.6 microseconds to settle - based on lab handout!
        self.voltage =  self.pin_read.read() #read voltage from the read pin
        #perform position calculation with linear calculations/coeffecients
        self.pos = int(self.voltage/self.edgeVoltage*self.screenLength_y - self.screenLength_y/2)
        return self.pos
    
    def scan_Z(self):
        '''
        @brief Scans the z status of the touch
        
        @details This function scans and returns the z status of the touch - that is,
        if a touch is being pressed or not. The pins need to be configured as follows
        to return the z status: 
         -  high pin = y_p
         -   low pin = x_m
         - highz pin = x_p
         -  read pin = y_m
        '''
        # Shuffle pins to correct modes
        self.pin_high = pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.y_m)
        self.pin_highz = pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        self.pin_high.value(1) #set high pin to high
        self.pin_low.value(0)  #set low pin to low
        utime.sleep_us(4)      #3.6 microseconds to settle - based on lab handout!
        return bool(self.pin_read.read() < self.liftVoltage)
    
    def scan_All(self): #Optimized for speed (less pin shuffling)
        '''
        @brief Returns all positions of touch as fast as possible.
        
        @details Coded with speed in mind, this function reads the data from 
        each pin as quickly as possible, and also prevents reshuffling of pins when they
        actually do not change. The logic follows that of scan_X, scan_Y, and scan_Z --
        but they are scanned in a matter which requires the least amount of reshuffling.
        In my findings, I saw that scanning Z then scanning Y and then scanning X is
        the fastest case. If Z is false, there is no need to scan for X and Y
        '''
        # Read Z first -- reshuffle all pins to starting
        self.pin_high = pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.y_m)
        self.pin_highz = pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        self.pin_high.value(1) #set high pin to high
        self.pin_low.value(0)  #set low pin to low
        utime.sleep_us(4)      #3.6 microseconds to settle - based on lab handout!
        self.z = bool(self.pin_read.read() < self.liftVoltage)
        
        if self.z == True:
            # Shuffle pins to correct modes
            self.pin_low = pyb.Pin(self.y_m, mode = pyb.Pin.OUT_PP)
            self.pin_read = pyb.ADC(self.x_p)
            self.pin_highz = pyb.Pin(self.x_m, mode = pyb.Pin.IN)
            self.pin_high.value(1) #set high pin to high
            self.pin_low.value(0)  #set low pin to low
            utime.sleep_us(4)      #3.6 microseconds to settle - based on lab handout!
            self.voltage =  self.pin_read.read() #read voltage from the read pin
            #perform position calculation with linear calculations/coeffecients
            self.pos = int(self.voltage/self.edgeVoltage*self.screenLength_y - self.screenLength_y/2)
            self.y = self.pos
            
            # Shuffle pins to correct modes
            self.pin_high = pyb.Pin(self.x_p, mode = pyb.Pin.OUT_PP)
            self.pin_low = pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
            self.pin_read = pyb.ADC(self.y_p)
            self.pin_highz = pyb.Pin(self.y_m, mode = pyb.Pin.IN)
            self.pin_high.value(1) #set high pin to high
            self.pin_low.value(0)  #set low pin to low
            utime.sleep_us(4)      #3.6 microseconds to settle - based on lab handout!
            self.voltage =  self.pin_read.read()
            #perform position calculation with linear calculations/coeffecients
            self.pos = int(self.voltage/self.edgeVoltage*self.screenLength_x - self.screenLength_x/2)
            self.x = self.pos
            
        else:
            self.x = 0
            self.y = 0
        
        return (self.x, self.y, self.z)

#-----------------------------------TESTING CODE-----------------------------#
if __name__ == "__main__":
    #create the touchscreen object with my touchscreen driver
    myTouchScreen = TouchScreen(pyb.Pin.cpu.A0, pyb.Pin.cpu.A1, pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)
    
    #indefinetely update the touchscreen, and print the results and timing.
    while True:
        t_init = utime.ticks_us() #start of recording
        (x,y,z) = myTouchScreen.scan_All() #gather data (optimized stream)
        delta = utime.ticks_us() - t_init #recording time
        print('time: {} [us], Touch Detected: {}, X: {} [mm], Y: {} [mm]'.format(delta,z,x,y))